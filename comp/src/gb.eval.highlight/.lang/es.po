#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: ~ 3.18.90\n"
"PO-Revision-Date: 2023-06-08 07:24 UTC\n"
"Last-Translator: Martin Belmonte <info@belmotek.net>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

# gb-ignore
#: .project:2
msgid "~"
msgstr "-"

#: Highlight.class:27
msgid "Background"
msgstr "Fondo"

#: Highlight.class:27
msgid "Normal text"
msgstr "Texto normal"

#: Highlight.class:27
msgid "Selection background"
msgstr "Fondo de selección"

#: Highlight.class:27
msgid "Alternate background"
msgstr "Fondo alternativo"

#: Highlight.class:27
msgid "Highlighting"
msgstr "Resaltado"

#: Highlight.class:27
msgid "Current line"
msgstr "Línea actual"

#: Highlight.class:27
msgid "Added text"
msgstr "Texto añadido"

#: Highlight.class:27
msgid "Removed text"
msgstr "Texto eliminado"

#: Highlight.class:27
msgid "Errors"
msgstr "Errores"

#: Highlight.class:27
msgid "Comments"
msgstr "Comentarios"

#: Highlight.class:27
msgid "Documentation"
msgstr "Documentación"

#: Highlight.class:27
msgid "Keywords"
msgstr "Palabras clave"

#: Highlight.class:27
msgid "Functions"
msgstr "Funciones"

#: Highlight.class:27
msgid "Operators"
msgstr "Operadores"

#: Highlight.class:27
msgid "Symbols"
msgstr "Símbolos"

#: Highlight.class:27
msgid "Numbers"
msgstr "Números"

#: Highlight.class:27
msgid "Strings"
msgstr "Cadenas"

#: Highlight.class:27
msgid "Breakpoints"
msgstr "Puntos de interrupción"

#: Highlight.class:27
msgid "Execution line"
msgstr "Línea de ejecución"

#: Highlight.class:27
msgid "Datatypes"
msgstr "Tipos de datos"

#: Highlight.class:27
msgid "Preprocessor"
msgstr "Preprocesador"

#: Highlight.class:27
msgid "Escaped characters"
msgstr "Caracteres escapados"

#: Highlight.class:27
msgid "Labels"
msgstr "Etiquetas"

#: Highlight.class:27
msgid "Constants"
msgstr "Constantes"

#: Highlight.class:27
msgid "Class selectors"
msgstr "Selectores de clase"

#: Highlight.class:27
msgid "Id selectors"
msgstr "Selectores de Id"

#: Highlight.class:27
msgid "Element selectors"
msgstr "Selectores de elementos"

#: Highlight.class:27
msgid "Properties"
msgstr "Propiedades"

#: Highlight.class:27
msgid "CSS pseudo-classes"
msgstr "Pseudoclases CSS"

#: Highlight.class:27
msgid "CSS rules"
msgstr "Reglas CSS"

#: Highlight.class:27
msgid "Important values"
msgstr "Valores importantes"

#: Highlight.class:27
msgid "File names"
msgstr "Nombres de archivo"

#: Highlight.class:27
msgid "Header"
msgstr "Encabezado"

#: Highlight.class:27
msgid "Positions"
msgstr "Posiciones"

#: Highlight.class:27
msgid "Markups"
msgstr "Marcas"

#: Highlight.class:27
msgid "Attributes"
msgstr "Atributos"

#: Highlight.class:27
msgid "Values"
msgstr "Valores"

#: Highlight.class:27
msgid "Entities"
msgstr "Entidades"

#: Highlight.class:27
msgid "Webpage markups"
msgstr "Marcas de página web"

#: Highlight.class:27
msgid "Webpage comments"
msgstr "Comentarios de la página web"

#: Highlight.class:27
msgid "Webpage markup arguments"
msgstr "Argumentos de marcado de páginas web"

