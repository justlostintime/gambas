Format: 3.0 (quilt)
Source: gambas3
Binary: @{dsc-binary}@
Architecture: any all
Version: 0.0.1
Maintainer: Willy Raets <gbWilly@protonmail.com>
Uploaders: Willy Raets <gbWilly@protonmail.com>
Homepage: @{website-url}@
Standards-Version: 4.5.0
Vcs-Browser: @{git-url}@
Vcs-Git: @{git-repository}@
@{dsc-build-depends}@
@{dsc-package-list}@
Files:
 437a8d1df895203af8b08e64717a6e30 2893892 gambas3-@{version}@@{source-version}@.orig.tar.bz2
 0323ebdd4a1983de61a5e4f326194426 20710 gambas3-@{version}@@{source-version}@.debian.tar.gz
DEBTRANSFORM-TAR: gambas-@VERSION@.tar.gz
DEBTRANSFORM-FILES-TAR: @{control-file}@
