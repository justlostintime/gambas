/***************************************************************************

	gba.c

	(c) 2000-2017 Benoît Minisini <benoit.minisini@gambas-basic.org>

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2, or (at your option)
	any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	MA 02110-1301, USA.

***************************************************************************/

#define __GBA_C

#include "config.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <dirent.h>

#include "gb_common.h"
#include "gb_error.h"
#include "gb_str.h"
#include "gb_file.h"
#include "gb_array.h"
#include "gb_common_buffer.h"
#include "gbc_project.h"
#include "gbc_archive.h"

#if HAVE_GETOPT_LONG
static struct option Long_options[] =
{
	{ "extract", 1, NULL, 'x' },
	{ "help", 0, NULL, 'h' },
	{ "ignore-public", 0, NULL, 'p' },
	{ "list", 1, NULL, 'l' },
	{ "output", 1, NULL, 'o' },
	{ "verbose", 0, NULL, 'v' },
	{ "version", 0, NULL, 'V' },
	{ "swap", 0, NULL, 's' },
	{ 0 }
};
#endif

static char **path_list;
static int path_current;

static const char *_allowed_hidden_files[] = { ".gambas", ".info", ".list", ".test", ".lang", ".action", ".connection", ".component", ".public", ".app.png", NULL };
#define PUBLIC_DIR _allowed_hidden_files[8]

static const char *remove_ext_lang[] = { "pot", "po", NULL };

static bool _extract = FALSE;
static char *_extract_file = NULL;

static bool _list_all = FALSE;

static char *_archive;

static bool _ignore_public = FALSE;

static char **_ignore_paths = NULL;

static void print_version()
{
	#ifdef TRUNK_VERSION
	printf(VERSION " " TRUNK_VERSION "\n");
	#else /* no TRUNK_VERSION */
	printf(VERSION "\n");
	#endif
}

static void print_title()
{
	printf("\nGambas archiver version ");
	print_version();
}

static void add_ignore_path_len(const char *path, int len)
{
	//fprintf(stderr, "add_ignore_path_len: %.*s\n", len, path);
	if (!_ignore_paths)
		ARRAY_create(&_ignore_paths);
	*((char **)ARRAY_add(&_ignore_paths)) = STR_copy_len(path, len);
}

static void add_ignore_path_recursive(const char *path, int len)
{
	int nfile;
	struct dirent **filelist = NULL;
	char *dir;
	int i;
	char *name;
	
	if (len <= 5 || strncmp(path, ".src/", 5))
	{
		add_ignore_path_len(path, len);
		return;
	}
	
	dir = STR_copy_len(path, len);
	
	nfile = scandir(dir, &filelist, NULL, alphasort);
	
	if (nfile < 0)
	{
		ERROR_warning("cannot scan directory: %.*s", len, path);
	}
	else
	{
		for (i = 0; i < nfile; i++)
		{
			name = filelist[i]->d_name;
			
			if (strcmp(name, ".") == 0 || strcmp(name, "..") == 0)
				continue;
			
			path = FILE_cat(dir, name, NULL);
			
			if (FILE_is_dir(path))
			{
				add_ignore_path_recursive(path, strlen(path));
			}
			else
			{
				if (strcmp(FILE_get_ext(path), "class") == 0)
				{
					name = STR_upper(FILE_get_basename(path));
					path = FILE_cat(".gambas", name, NULL);
					
					add_ignore_path_len(path, strlen(path));
					
					STR_free(name);
				}
			}
		}
		
		for (i = 0; i < nfile; i++)
			free(filelist[i]);
		free(filelist);
	}
	
	STR_free(dir);
}

static bool add_ignore_paths_from_project(const char *key, int len_key, const char *value, int len_value)
{
	if (!PROJECT_is_key(key, len_key, "IgnoreFiles"))
		return FALSE;
	
	PROJECT_browse_string_list(value, len_value, add_ignore_path_recursive);
	return TRUE;
}

static bool must_ignore_path(const char *path)
{
	int i;

	if (!_ignore_paths || !path || !*path)
		return FALSE;

	for (i = 0; i < ARRAY_count(_ignore_paths); i++)
	{
		if (!strcmp(_ignore_paths[i], path))
			return TRUE;
	}

	return FALSE;
}

static void get_arguments(int argc, char **argv)
{
	int opt;
	#if HAVE_GETOPT_LONG
	int index = 0;
	#endif

	for(;;)
	{
		#if HAVE_GETOPT_LONG
			opt = getopt_long(argc, argv, "vVLhso:x:l:pi:", Long_options, &index);
		#else
			opt = getopt(argc, argv, "vVLhso:x:l:pi:");
		#endif

		if (opt < 0) break;

		switch (opt)
		{
			case 'V':
				print_version();
				exit(0);

			case 'v':
				ARCH_verbose = TRUE;
				break;

			case 's':
				ARCH_swap = TRUE;
				break;

			case 'o':
				ARCH_define_output(optarg);
				break;
				
			case 'x':
				_archive = optarg;
				_extract = TRUE;
				break;

			case 'l':
				_archive = optarg;
				_list_all = TRUE;
				break;

			case 'p':
				_ignore_public = TRUE;
				break;

			case 'L':
				print_version();
				printf(COPYRIGHT);
				exit(0);

			case 'h': case '?': case ':':
				print_title();
				printf(
					"\nCreate a standalone one-file executable from a Gambas project.\n"
					"\n    gba" GAMBAS_VERSION_STRING " [options] [<project directory>]\n"
					"\nExtract a specific file from a Gambas executable (-x option).\n"
					"\n    gba" GAMBAS_VERSION_STRING " -x <archive path> <file>\n"
					"\nList all files included in a Gambas executable (-l option).\n"
					"\n    gba" GAMBAS_VERSION_STRING " -l <archive path>\n\n"
					"Options:"
					#if HAVE_GETOPT_LONG
					"\n\n"
					"  -h  --help                 display this help\n"
					"  -l  --list=ARCHIVE         list archive files\n"
					"  -L  --license              display license\n"
					"  -o  --output=ARCHIVE       archive path [<project directory>/<project name>.gambas]\n"
					"  -p  --ignore-public        exclude the '.public' directory\n"
					"  -s  --swap                 swap endianness\n"
					"  -v  --verbose              verbose output\n"
					"  -V  --version              display version\n"
					"  -x  --extract=ARCHIVE      extract a specific file from the archive\n"
					#else
					" (no long options on this system)\n\n"
					"  -h                     display this help\n"
					"  -l=ARCHIVE             list archive files\n"
					"  -L                     display license\n"
					"  -o=ARCHIVE             archive path [<project directory>/<project name>.gambas]\n"
					"  -p                     exclude the '.public' directory\n"
					"  -s                     swap endianness\n"
					"  -v                     verbose output\n"
					"  -V                     display version\n"
					"  -x=ARCHIVE             extract a specific file from the archive\n"
					#endif
					"\n"
					);

				exit(opt != 'h');

			default:
				exit(1);

		}
	}

	if (optind < (argc - 1))
	{
		ERROR_warning("too many arguments");
		exit(1);
	}

	if (_extract)
	{
		if (optind == argc)
		{
			ERROR_warning("not enough arguments");
			exit(1);
		}
		_extract_file = argv[optind];
	}
	else if (_list_all)
	{
		// everything is ok
	}
	else
	{
		if (optind == argc)
			ARCH_define_project(NULL);
		else
			ARCH_define_project(argv[optind]);

		if (!FILE_exist(ARCH_project))
		{
			ERROR_warning("project file not found: %s", ARCH_project);
			exit(1);
		}
	}
}


static void print_resolved_path_rec(ARCH *arch, int n)
{
	static char buffer[16];

	const char *name = arch->symbol[n].sym.name;
	int len = arch->symbol[n].sym.len;
	int nrec;
	const char *p = NULL;

	if (*name == '/')
	{
		p = index(name, ':');
		if (p)
		{
			nrec = p - name - 1;
			if (nrec > 0 && nrec < 16)
			{
				strncpy(buffer, &name[1], nrec);
				buffer[nrec] = 0;
				nrec = atoi(buffer);
				if (nrec > 0 && nrec < arch->header.n_symbol)
				{
					print_resolved_path_rec(arch, nrec);
					putchar('/');
				}
			}
		}
	}

	if (p)
		fwrite(p + 1, sizeof(char), len - (p - name) - 1, stdout);
	else
		fwrite(name, sizeof(char), len, stdout);
}


static void print_resolved_path(ARCH *arch, int n)
{
	print_resolved_path_rec(arch, n);
	putchar('\n');
}


static void path_add(const char *path)
{
	*((char **)ARRAY_add(&path_list)) = STR_copy(path);
}


static void path_init(const char *first)
{
	ARRAY_create(&path_list);

	if (*first)
		FILE_chdir(first);

	path_add(FILE_get_current_dir());

	path_current = 0;
}


static void path_exit(void)
{
	ARRAY_delete(&path_list);
}


static int path_count(void)
{
	return ARRAY_count(path_list);
}


static void ignore_exit(void)
{
	int i;

	if (!_ignore_paths)
		return;

	for (i = 0; i < ARRAY_count(_ignore_paths); i++)
		STR_free(_ignore_paths[i]);

	ARRAY_delete(&_ignore_paths);
}

int main(int argc, char **argv)
{
	const char *path;
	int nfile;
	struct dirent **filelist;
	struct dirent *dirent;
	char *file_name;
	const char *file;
	struct stat info;
	const char *ext;
	int len;
	const char **p;
	int len_prefix;
	const char **remove_ext;
	ARCH *arch;
	ARCH_FIND find;
	int i;

	ERROR_init("gba" GAMBAS_VERSION_STRING ": ");

	get_arguments(argc, argv);

	COMMON_init();

	TRY
	{
		if (_extract) // Extract a file from an archive
		{
			arch = ARCH_open(_archive);
			
			if (ARCH_find(arch, _extract_file, 0, &find))
				ERROR_warning("file not found in archive");
			else
				fwrite(&arch->addr[find.pos], sizeof(char), find.len, stdout);
			
			ARCH_close(arch);
		}
		else if (_list_all)
		{
			arch = ARCH_open(_archive);

			for (i = 0; i < arch->header.n_symbol; i++)
				print_resolved_path(arch, i);

			ARCH_close(arch);
		}
		else // Create an archive
		{
			ARCH_init();

			file = FILE_get_dir(ARCH_project);
			len_prefix = strlen(file);
			path_init(file);

			// '.startup', '.project' and '.environment' files are always first!
			
			path = FILE_cat(FILE_get_dir(ARCH_project), ".startup", NULL);
			if (FILE_exist(path)) ARCH_add_file(path);

			path = FILE_cat(FILE_get_dir(ARCH_project), ".project", NULL);
			if (FILE_exist(path)) 
			{
				ARCH_add_file(path);
				PROJECT_load(path);
				PROJECT_browse(add_ignore_paths_from_project);
			}

			path = FILE_cat(FILE_get_dir(ARCH_project), ".environment", NULL);
			if (FILE_exist(path)) ARCH_add_file(path);

			for(;;)
			{
				if (path_current >= path_count())
					break;

				path = path_list[path_current++];
				
				if (chdir(path) != 0)
				{
					ERROR_warning("warning: cannot change to directory: %s", path);
					goto _NEXT_PATH;
				}

				filelist = NULL;
				nfile = scandir(path, &filelist, NULL, alphasort);

				if (nfile < 0)
				{
					ERROR_warning("warning: cannot scan directory: %s", path);
					goto _NEXT_PATH;
				}
				
				for (i = 0; i < nfile; i++)
				{
					dirent = filelist[i];
					
					file_name = dirent->d_name;
					len = strlen(file_name);

					file = FILE_cat(path, file_name, NULL);

					if (must_ignore_path(&file[len_prefix + 1]))
						continue;

					if (*file_name == '.')
					{
						// hidden files are allowed only on the root of the project
						if (path_current != 1)
							continue;

						for (p = _allowed_hidden_files; *p; p++)
						{
							if (strcmp(file_name, *p) == 0)
								break;
						}

						if (*p == NULL)
							continue;

						if (*p == PUBLIC_DIR && _ignore_public)
							continue;
					}

					if (file_name[len - 1] == '~')
						continue;

					//if (strcmp(file_name, ARCH_project_name) == 0)
					//  continue;
						
					if ((len == 4) && (strncmp(file_name, "core", 4) == 0))
						continue;

					if ((len > 5) && (strncmp(file_name, "core.", 5) == 0))
						continue;

					if ((len > 7) && (strncmp(file_name, "vgcore.", 5) == 0))
						continue;

					if ((len > 10) && (strncmp(file_name, "callgrind.", 5) == 0))
						continue;

					// Do not put the archive file inside itself.
					if (!strcmp(file, ARCH_output))
						continue;

					if (stat(file_name, &info))
					{
						ERROR_warning("warning: cannot stat file: %s", file);
						continue;
					}

					if (S_ISDIR(info.st_mode))
					{
						if (strcmp(file_name, "CVS") == 0)
							continue;
							
						path_add(file);
						ARCH_add_file(file);
					}
					else
					{
						ext = FILE_get_ext(file_name);

						//printf("path = %s\n", &path[len_prefix]);

						//if (path[len_prefix] == 0)
						//	remove_ext = remove_ext_root;
						if (strcmp(&path[len_prefix], "/.lang") == 0)
							remove_ext = remove_ext_lang;
						else
							remove_ext = 0;
						
						if (remove_ext)
						{
							for (p = remove_ext; *p; p++)
							{
								if (strcasecmp(ext, *p) == 0)
									break;
							}
						
							if (*p != NULL)
								continue;
						}
						
						if (strcmp(ext, "gambas") == 0)
							continue;

						ARCH_add_file(file);
					}
				}

	_NEXT_PATH:
	
				if (filelist != NULL) 
				{
					for (i = 0; i < nfile; i++)
						free(filelist[i]);
					free(filelist);
				}
					
				FREE((char **)&path);
			}

			path_exit();
			ignore_exit();
			ARCH_exit();
			PROJECT_exit();
			/*MEM_check();*/
		}
	}
	CATCH
	{
		fflush(NULL);
		fprintf(stderr, "gba" GAMBAS_VERSION_STRING ": error: ");
		ERROR_print();
		exit(1);
	}
	END_TRY

	return 0;
}

